"""
Utils for scraping.
"""


import bs4
from bs4.element import Comment
import requests
from time import sleep
import matplotlib.pyplot as plt
import networkx as nx
# see Bird, Steven, Edward Loper and Ewan Klein (2009), Natural Language Processing with Python. O’Reilly Media Inc.
import nltk
from nltk.corpus import stopwords
import string

def get_soup(thisurl):
    thisscrape = requests.get(thisurl)
    return bs4.BeautifulSoup(thisscrape.text, 'html.parser')

def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True

# thanks to https://stackoverflow.com/a/1983219/5599687
def get_body_text(thisurl):
    soup = get_soup(thisurl)
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    return u" ".join(t.strip() for t in visible_texts)

    

class Words:
    """
    Class for handling word counts and statistics.
    """
    def __init__(self, get_from_url=None):
        self.list_of_words = []
        self.unuseful_words = set(stopwords.words('english'))
        if get_from_url:
            self.add_text(' '.join(get_soup(get_from_url).find_all(string=True)))

    def add_text(self, input_text):
        input_list = input_text.split()
        formatted_list = []
        for inp in input_list:
            # https://www.stechies.com/remove-punctuation-from-string-python/
            formatted_list.append(inp.replace('\n','').strip().lower().translate(str.maketrans('', '', string.punctuation)))
        for uw in self.unuseful_words:
            while uw in formatted_list:
                formatted_list.remove(uw)
        #for inp in formatted_list:
        #    if not inp.isalpha():
        #        formatted_list.remove(inp)
        #print(input_list)
        self.list_of_words.extend(formatted_list)

    def sorted_words(self):
        h_words = {}
        for w in self.list_of_words:
            #if w.isalpha():
            h_words[w] = h_words.get(w,0) + 1
    
        hsort = sorted(h_words.items(), key=lambda item: item[1])[::-1]
        return hsort


    def plot_freqs(self):
        """
        Plots frequencies of words. Returns the sorted list.
        """
        hlist = self.sorted_words()[:20]
        hx = [hs[0] for hs in hlist]
        hy = [hs[1] for hs in hlist]
        plt.figure(figsize=(20,10))
        plt.xticks(rotation=90)
        plt.bar(hx,hy)
        plt.show()
        return hsort

def draw_community(graph, comm):
    """
    Plots the network `graph` coloring according to `comm`.
    """
    plt.figure()
    for com in c:
        clist = list(com)
        #if not isinstance(clist[0], str):
        if True:
            for n in com:
                graph.nodes[n]['col'] = c.index(com)
    #return [nv.get('col') for nv in graph.node_dict_factory(graph.nodes).values()]
    nx.draw(graph, with_labels=True, node_color=[nv.get('col') for nv in graph.node_dict_factory(graph.nodes).values()])



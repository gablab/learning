"""
Following https://nlp.stanford.edu/IR-book/pdf/irbookonlinereading.pdf (chapter 6 in particular).
"""

import string
import math
# see Bird, Steven, Edward Loper and Ewan Klein (2009), Natural Language Processing with Python. O’Reilly Media Inc.

from nltk.corpus import stopwords
import numpy as np
import regex as re

def words_from_text(text):
    """
    Return a list of lowercase words in the text

    >>> words_from_text(' chi va con lo zoppo,     a volte, dice “ciao Erik”... ed è rosso')
    ['chi', 'va', 'con', 'lo', 'zoppo', 'a', 'volte', 'dice', 'ciao', 'erik', 'ed', 'è', 'rosso']
    >>> words_from_text('1.000.000')
    ['1000000']
    >>> words_from_text(None)
    ['']
    """
    pattern = re.compile('[\W_]+')
    if not text:
        return ['']
    return (pattern.sub(' ', text.translate(str.maketrans(' ', ' ', string.punctuation))).replace('\n',' ').strip().lower().split())

def term_frequency(term, text):
    """
    Returns the tf of `term` in `text`.

    >>> term_frequency('ciao', 'ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?')
    3
    """
    return words_from_text(text).count(term)

def document_frequency(term, docs):
    """ 
    Returns the df of `term` in the collection `docs`.

    >>> document_frequency('ciao', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    2
    """
    df = 0
    for doc in docs:
        if doc.count(term) > 0:
            df += 1
    return df 

def inverse_document_frequency(term, docs):
    """ 
    Returns the idf of `term` in the collection `docs`.

    >>> inverse_document_frequency('ciao', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    0.4054651081081644
    >>> inverse_document_frequency('Asdrubale', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    0
    """
    if(document_frequency(term, docs) == 0):
        return 0
    return math.log(float(len(docs))/document_frequency(term, docs))

def tf_idf(term, text, docs):
    """ 
    Returns the tf-idf weight of `term` in the collection `docs`.

    >>> tf_idf('ciao', 'ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    1.2163953243244932
    >>> tf_idf('Asdrubale', 'ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    0
    """
    return term_frequency(term, text) * inverse_document_frequency(term, docs)

def vector_score(query, text, docs):
    """ 
    Returns the normalized array of tf-idf weights of `query` for `text` in the collection `docs`.

    >>> vector_score('ciao Asdrubale Venezia', 'ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    array([1., 0., 0.])
    """
    uniques = []
    for w in words_from_text(query):
        if w not in uniques:
            uniques.append(w)
    vector = np.array([tf_idf(q, text, docs) for q in uniques])
    norm = np.linalg.norm(vector)
    if norm == 0.0:
        return 0.0
    else:
        return vector/norm

def similarity_score(text1, text2, docs):
    """
    Returns the similarity score (from 0 to 1) between `text1` and `text2` in the collection `docs`, with a cosine similarity approach using tf_idf score.

    >>> similarity_score('dimmi «ciao» e parti', 'dimmi «ciao» e parti', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    1.0
    >>> similarity_score('', '', ['ciao, mi chiamo Gabriele e dico «Ciao!». Chi altro fa "ciao" con la mano?', 'Quanti siamo in macchina?', 'dimmi «ciao» e parti'])
    0.0
    """
    # the query contains all the words in both documents
    query = text1+' '+text2
    for sw in set(stopwords.words('english')):
        q = query.replace(sw, '')
        t1 = text1.replace(sw, '')
        t2 = text2.replace(sw, '')
    return np.dot(vector_score(q, t1, docs), vector_score(q, t2, docs))


def calc_affinity(text, categ_dict):
    """
    Returns a dictionary with the similarity scores associated to the
    categories given in `categ_dict`, which is a dictionary of the form 
    `{category: sample}`.

    >>> sorted(calc_affinity('Mario e Luigi vivono in una casa e hanno possedimenti', {'animali': 'cani e gatti sono animali che si trovano nelle case', 'persone': 'le persone possiedono oggetti artefatti, vivono nelle case', 'oggetti': 'gli oggetti sono utili per compiere azioni'}).items(), key=lambda x: x[1])[-1][0]
    'persone'
    >>> sorted(calc_affinity('cani e gatti sono matti', {'animali': 'cani e gatti sono animali che si trovano nelle case', 'persone': 'le persone possiedono oggetti artefatti, vivono nelle case', 'oggetti': 'gli oggetti sono utili per compiere azioni'}).items(), key=lambda x: x[1])[-1][0]
    'animali'
    """
    docs = [c for c in categ_dict.values()]
    affinity_dict = {}
    for k, v in categ_dict.items():
        affinity_dict[k] = similarity_score(text, v, docs)
    return affinity_dict





if __name__ == '__main__':
    import doctest
    doctest.testmod()
    print('end')


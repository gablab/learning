program main

  use parallel_io
  implicit none

  character(100) :: filename
  integer :: comm, info, fh, ierror
  integer(kind=mpi_offset_kind) :: displacement
  integer :: stat(MPI_STATUS_SIZE)
  integer :: rank, nproc
  integer :: int_size

  integer :: readsize 
  integer, dimension(:), allocatable :: readbuf

  double precision, dimension(20) :: myarr = (/ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 /)
  double precision, dimension(20) :: rdarr

  call parallel_io_on 
  call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)

  filename = 'test.dat'

  call parallel_write_array_double(myarr,trim(filename))

  call parallel_read_array_double(rdarr,trim(filename))
  if(rank.eq.0) print*, rdarr


  call parallel_io_off

contains


end program

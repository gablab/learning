module parallel_io
  use mpi
  implicit none 


contains

  subroutine parallel_io_on()
    integer :: ierror 
    call mpi_init(ierror)
  end subroutine parallel_io_on

  subroutine parallel_io_off()
    integer :: ierror 
    call mpi_finalize(ierror)
  end subroutine parallel_io_off

  subroutine parallel_read_array_double(array,filename)
    implicit none
    double precision, dimension(:), intent(inout) :: array
    character(len=*), intent(in) :: filename
    integer :: comm, info, fh, ierror, amode
    integer(kind=mpi_offset_kind) :: displacement
    integer :: stat(MPI_STATUS_SIZE)
    integer :: rank, nproc
    integer :: data_size
    integer :: readsize 

    call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
    call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)

    info = MPI_INFO_NULL
    comm = MPI_COMM_WORLD

    amode = ior(MPI_MODE_RDWR,MPI_MODE_CREATE)
    call mpi_file_open(comm,trim(filename),amode,info,fh,ierror)
    if(rank.eq.0) then
      readsize = size(array) 
      call mpi_file_read(fh,array,readsize,MPI_DOUBLE_PRECISION,stat,ierror)
      !call mpi_file_read_at(fh,0,array,readsize,MPI_DOUBLE_PRECISION,stat,ierror)
    end if
    call mpi_file_close(fh,ierror)
  end subroutine parallel_read_array_double



  subroutine parallel_write_array_double(array,filename)
    implicit none
    double precision, dimension(:), intent(in) :: array
    character(len=*), intent(in) :: filename
    integer :: comm, info, fh, ierror, amode
    integer(kind=mpi_offset_kind) :: displacement
    integer :: stat(MPI_STATUS_SIZE)
    integer :: rank, nproc
    integer :: data_size
    integer :: bufsize 
    double precision, dimension(:), allocatable :: buf

    call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
    call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)

    bufsize = size(array)/nproc
    allocate(buf(bufsize))
    buf = array(rank*bufsize+1:(rank+1)*bufsize)
    info = MPI_INFO_NULL
    comm = MPI_COMM_WORLD


    call mpi_type_size(MPI_DOUBLE_PRECISION,data_size,ierror)
    displacement = rank * data_size * bufsize
    amode = ior(MPI_MODE_RDWR,MPI_MODE_CREATE)
    call mpi_file_open(comm,trim(filename),amode,info,fh,ierror)
    call mpi_file_write_at(fh,displacement,buf,bufsize,MPI_DOUBLE_PRECISION,stat,ierror)
    call mpi_file_close(fh,ierror)

    deallocate(buf)
  end subroutine



  subroutine parallel_read_array_integer(array,filename)
    implicit none
    integer, dimension(:), intent(inout) :: array
    character(len=*), intent(in) :: filename
    integer :: comm, info, fh, ierror, amode
    integer(kind=mpi_offset_kind) :: displacement
    integer :: stat(MPI_STATUS_SIZE)
    integer :: rank, nproc
    integer :: data_size
    integer :: readsize 

    call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
    call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)

    info = MPI_INFO_NULL
    comm = MPI_COMM_WORLD

    amode = ior(MPI_MODE_RDWR,MPI_MODE_CREATE) ! bitwise Boolean inclusive-OR
    call mpi_file_open(comm,trim(filename),amode,info,fh,ierror)
    if(rank.eq.0) then
      readsize = size(array) 
      call mpi_file_read(fh,array,readsize,MPI_INTEGER,stat,ierror)
      !call mpi_file_read_at(fh,0,array,readsize,MPI_DOUBLE_PRECISION,stat,ierror)
    end if
    call mpi_file_close(fh,ierror)
  end subroutine parallel_read_array_integer


  subroutine parallel_write_array_integer(array,filename)
    implicit none
    integer, dimension(:), intent(in) :: array
    character(len=*), intent(in) :: filename
    integer :: comm, info, fh, ierror, amode
    integer(kind=mpi_offset_kind) :: displacement
    integer :: stat(MPI_STATUS_SIZE)
    integer :: rank, nproc
    integer :: data_size
    integer :: bufsize 
    integer, dimension(:), allocatable :: buf

    call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
    call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)

    bufsize = size(array)/nproc
    allocate(buf(bufsize))
    buf = array(rank*bufsize+1:(rank+1)*bufsize)
    info = MPI_INFO_NULL
    comm = MPI_COMM_WORLD


    call mpi_type_size(MPI_DOUBLE_PRECISION,data_size,ierror)
    displacement = rank * data_size * bufsize
    amode = ior(MPI_MODE_RDWR,MPI_MODE_CREATE)
    call mpi_file_open(comm,trim(filename),amode,info,fh,ierror)
    call mpi_file_write_at(fh,displacement,buf,bufsize,MPI_DOUBLE_PRECISION,stat,ierror)
    call mpi_file_close(fh,ierror)

    deallocate(buf)
  end subroutine parallel_write_array_integer

end module

module solve_diffusion
  implicit none

contains

  subroutine solve_diff(phi,phi_t,phi_l,phi_b,phi_r)
    ![ takes phi and the shadow arrays
    double precision, intent(inout), dimension(:,:) :: phi
    double precision, intent(inout), dimension(:) :: phi_t,phi_l,phi_b,phi_r
    integer :: i,j, imax, jmax
    double precision :: tmpi,tmpj
    imax = size(phi,1)
    jmax = size(phi,2)
    do j=1,jmax
      do i=1,imax
        if(i.eq.1) then
          tmpi = phi_l(j) + phi(2,j) 
        else if(i.eq.imax) then
          tmpi = phi(imax-1,j) + phi_r(j)
        else
          tmpi = phi(i-1,j) + phi(i+1,j)
        end if
        if(j.eq.1) then
          tmpj = phi_b(i) + phi(i,2) 
        else if(j.eq.jmax) then
          tmpj = phi(i,jmax-1) + phi_t(i)
        else
          tmpj = phi(i,j-1) + phi(i,j+1)
        end if
        phi(i,j) = (tmpi + tmpj)/4.
      end do
    end do
  end subroutine

end module solve_diffusion

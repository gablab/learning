module mpi_diffusion
  use solve_diffusion
  use mpi
  implicit none 
  double precision, dimension (:,:), allocatable :: phi, phi_total
  double precision, dimension (:), allocatable :: phi_L, phi_R, phi_T, phi_B
  integer :: it
  integer, dimension (2) :: sizes, sizes_tot
  integer :: comm, info, fh, ierror, amode
  integer :: rank, nproc, crank
  integer :: mpi_comm_cartesian
  integer, dimension(2) :: cart_dims
  logical, dimension(2) :: cart_period
  logical :: cart_reord = .true.

contains

  subroutine init(N,M)
    integer, intent(in) :: N,M
    integer, dimension(2) :: coords
    call mpi_init(ierror)
    call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
    call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)
    cart_dims = 0
    call MPI_DIMS_CREATE(nproc,2,cart_dims,ierror)
    cart_period = .false.
    cart_reord = .true.
    call mpi_cart_create(MPI_COMM_WORLD,2,cart_dims,cart_period,cart_reord,mpi_comm_cartesian,ierror)
    call mpi_comm_rank(mpi_comm_cartesian, crank, ierror)
    sizes(1) = N
    sizes(2) = M
    !print*,'cart dims', rank,cart_dims
    sizes_tot(1) = sizes(1)*cart_dims(1)
    sizes_tot(2) = sizes(2)*cart_dims(2)
    allocate(phi(N,M))
    allocate(phi_T(N),phi_B(N),phi_L(M),phi_R(M))
    phi(:,:) = 0.0
    call boundary()
    it = 0
    !print*,'init bounded',rank
    if(crank.eq.0) then
      allocate(phi_total(sizes_tot(1),sizes_tot(2)))
    end if
  end subroutine

  subroutine step()
    call boundary()
    !print*,'pBound',rank,it
    call solve_diff(phi,phi_t,phi_l,phi_b,phi_r)
    !print*,'pAvg',rank,it
  end subroutine

  subroutine boundary()
    implicit none
    integer, dimension(2) :: coords,tocoords
    logical, dimension(2) :: periods
    integer :: nn_t, nn_b, nn_l, nn_r ! nearest neighbors
    integer :: N,M
    N = sizes(1)
    M = sizes(2)
    call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
    call mpi_comm_rank(mpi_comm_cartesian, crank, ierror)
    call mpi_cart_coords(mpi_comm_cartesian,crank,2,coords,ierror)
    ! tag for sendrecv: right - 0, top - 1, left - 2, bottom - 3
    !MPI_SENDRECV(sendbuf,sendcount,sendtype,dest,sendtag,recvbuf,recvcount,recvtype,source,recvtag,comm,status)

    if(coords(2).eq.0) then ! bottom, given temperature
      phi_b(:) = 0.
    else ! communicate with bottom nearest neighbor
      tocoords = coords+(/0,-1/)
      call mpi_cart_rank(mpi_comm_cartesian,coords+(/0,-1/),nn_b,ierror)
     !print*,'to bottom',rank,'(',coords,'):',nn_b
      call mpi_sendrecv(phi(:,1),N,MPI_DOUBLE_PRECISION,nn_b,3, &
        phi_b,N,MPI_DOUBLE_PRECISION,nn_b,MPI_ANY_TAG,&
        mpi_comm_cartesian,MPI_STATUS_IGNORE,ierror)
    end if
    if(coords(2).eq.cart_dims(2)-1) then ! top, given temperature
      phi_t(:) = 1.
    else ! communicate with top nearest neighbor
      tocoords = coords+(/0,1/)
      call mpi_cart_rank(mpi_comm_cartesian,tocoords,nn_t,ierror)
      !print*,'to top',rank,'(',coords,'):',nn_t
      call mpi_sendrecv(phi(:,sizes(2)),N,MPI_DOUBLE_PRECISION,nn_t,1, &
        phi_t,N,MPI_DOUBLE_PRECISION,nn_t,MPI_ANY_TAG,&
        mpi_comm_cartesian,MPI_STATUS_IGNORE,ierror)
    end if
    if(coords(1).eq.0) then ! leftmost, free surface
      phi_l = phi(1,:) 
    else ! communicate with left nearest neighbor
      tocoords = coords+(/-1,0/)
      call mpi_cart_rank(mpi_comm_cartesian,tocoords,nn_l,ierror)
      !print*,'to left',rank,'(',coords,'):',nn_l
      call mpi_sendrecv(phi(1,:),M,MPI_DOUBLE_PRECISION,nn_l,2, &
        phi_l,N,MPI_DOUBLE_PRECISION,nn_l,MPI_ANY_TAG,&
        !phi_l,M,MPI_DOUBLE_PRECISION,nn_l,0,&
        mpi_comm_cartesian,MPI_STATUS_IGNORE,ierror)
    end if
    if(coords(1).eq.cart_dims(1)-1) then ! rightmost, free surface
      phi_r = phi(sizes(1),:)
    else ! communicate with right nearest neighbor
      tocoords = coords+(/1,0/)
      call mpi_cart_rank(mpi_comm_cartesian,coords+(/1,0/),nn_r,ierror)
      !print*,'to right',rank,'(',coords,'):',nn_r
      call mpi_sendrecv(phi(sizes(1),:),M,MPI_DOUBLE_PRECISION,nn_r,0, &
        !phi_r,M,MPI_DOUBLE_PRECISION,nn_r,2,&
        phi_r,N,MPI_DOUBLE_PRECISION,nn_r,MPI_ANY_TAG,&
        mpi_comm_cartesian,MPI_STATUS_IGNORE,ierror)
    end if

  end subroutine


  subroutine print_viz()
    implicit none
    integer :: i,j
    character(len=10) :: lf
    character(len=8)  :: str1, str2, str3
    character(len=16) :: str4
    character(len=80) :: buffer, namefile

    write(namefile,'(a,i8.8,a)') './output/OUTPAR_',it,'.dat'
    call open_newfile(76,trim(namefile))!,status='new',form='unformatted',access='stream',convert='big_endian')
    do j=1,sizes_tot(2)
      do i=1,sizes_tot(1)
        write(76,*) i,j,phi_total(:,j)
      end do
      write(76,*)
    end do


    ! end of line character
    lf=achar(10)
    !lf=achar(32)
    write(str1(1:8),'(i8)') sizes_tot(1)
    write(str2(1:8),'(i8)') sizes_tot(2)
    write(str3(1:8),'(i8)') 1
    write(str4(1:16),'(i16)') sizes_tot(1)*sizes_tot(2)*1
    write(namefile,'(a,i8.8,a)') './output/OUTPAR_',it,'.vtk'
    !open(66,file=trim(namefile),status='new')
    call open_newfile_unformatted(66,trim(namefile))!,status='new',form='unformatted',access='stream',convert='big_endian')
    ! write header
    buffer='# vtk DataFile Version 3.0'//lf
    write(66) trim(buffer)
    buffer='Field'//lf
    write(66) trim(buffer)
    !buffer='ASCII'//lf
    buffer='BINARY'//lf
    write(66) trim(buffer)
    buffer='DATASET RECTILINEAR_GRID'//lf
    write(66) trim(buffer)
    !write grid
    buffer='DIMENSIONS '//str1//str2//str3//lf
    write(66) trim(buffer)
    buffer='X_COORDINATES '//str1//'  double'//lf 
    write(66) trim(buffer)
    do i=1,sizes_tot(1)
      write(66) dble(i)
    enddo
    buffer='Y_COORDINATES '//str2//'  double'//lf 
    write(66) trim(buffer)
    do i=1,sizes_tot(1)
      write(66) dble(i)
    enddo
    buffer='Z_COORDINATES '//str3//'  double'//lf 
    write(66) trim(buffer)
    do i=1,1
      write(66) dble(i)
    enddo
    !write content (data format)
    buffer='POINT_DATA '//str4//lf
    write(66) trim(buffer)
    write(str1(1:8),'(i8)') 1 ! nfields
    buffer='FIELD FieldData '//str1//lf
    write(66) trim(buffer)
    buffer='phi 1 '//str4//'  double'//lf 
    write(66) trim(buffer)
    do j=1,sizes_tot(2)
      do i=1,sizes_tot(1)
        write(66) phi_total(i,j)
        !write(66,'(10F8.2)',advance='NO') phi(i,j)
      end do
    enddo
    close(66)
  end subroutine print_viz

  subroutine terminate
    deallocate(phi)
    if(crank.eq.0) deallocate(phi_total)
    call mpi_finalize(ierror)
    print*,'now terminate',rank
  end subroutine terminate

  subroutine gather_phi
    integer :: rk, j
    integer :: nproc_cart
    integer, dimension(2) :: coords
    double precision, dimension (:), allocatable :: phi_tot_tmp, phi_tmp
    !print*,'phi (',it,',',crank,'): [', minval(phi),':',maxval(phi),']'
    allocate(phi_tmp(sizes(1)*sizes(2)))
    if(crank.eq.0) then
      allocate(phi_tot_tmp(sizes_tot(1)*sizes_tot(2)))
      phi_tot_tmp = 0.0
      !print*,size(phi_tot_tmp)
    end if
    phi_tmp = reshape(phi,(/sizes(1)*sizes(2)/))
    !print*,'phi_tmp (',it,',',crank,'): [', minval(phi_tmp),':',maxval(phi_tmp),']'
    call mpi_gather(phi_tmp,sizes(1)*sizes(2),MPI_DOUBLE_PRECISION,&
      phi_tot_tmp,sizes(1)*sizes(2),MPI_DOUBLE_PRECISION,&
      0,mpi_comm_cartesian,ierror)
    !if(crank.eq.0) then
    !  print*,'before reshaping (',it,'): datarange = [',&
    !    minval(phi_tot_tmp),' : ',maxval(phi_tot_tmp),' ]'
    !end if
    if(crank.eq.0) then
      call mpi_comm_size(mpi_comm_cartesian, nproc_cart, ierror)
      !print*,phi_tot_tmp
      do rk=0,nproc_cart-1
        call mpi_cart_coords(mpi_comm_cartesian,rk,2,coords,ierror)
        call fill_matrix(phi_tot_tmp,phi_total,rk,coords(1),coords(2))
        do j=1,sizes(2)
            !!!phi_total(coords(1)*sizes(1):coords(1)*sizes(1)+sizes(1),&
            !!!  coords(2)*sizes(2)+j) = &
            !!!  phi_tot_tmp(mod(j,cart_dims(1))*cart_dims(1):mod(j,cart_dims(1))*cart_dims(1)+sizes(1),&
            !!!  int(j/cart_dims(1))+1)
      !call mpi_gather(phi(:,j),sizes(2),MPI_DOUBLE_PRECISION,&
      !  phi_total(coords(1)*sizes(1):coords(1)*sizes(1)+sizes(?),&
      !    coords(2)*sizes(2)+j),&
      !  sizes(2),MPI_DOUBLE_PRECISION,&
      !  0,mpi_comm_cartesian,ierror)
        end do
      end do
    end if
    !if(crank.eq.0) then
    !  print*,'gathered (',it,'): datarange = [',&
    !    minval(phi_total),' : ',maxval(phi_total),' ]'
    !end if
  end subroutine gather_phi

  subroutine fill_matrix(array,matrix,rk,x,y)
    double precision, dimension(:,:), intent(inout) :: matrix
    double precision, dimension(:),   intent(in)    :: array
    integer, intent(in) :: rk,x,y
    integer :: m,n,j
    integer :: start,aini,aend
    integer :: minix,mendx,my
    integer :: miniy,mendy,mx
    m = sizes(1)
    n = sizes(2)
    start = rk*m*n+1
    do j=1,sizes(2)
      aini = start+(j-1)*m
      aend = aini + m-1
      minix = m*x +1
      mendx = m*(x+1) 
      my = n*y+j-1 +1
      !mx = n*x+j-1 +1
      !miniy = m*y +1
      !mendy = m*(y+1) +1
      matrix(minix:mendx, my) = array(aini:aend)
      !print*, rk,'j=',j,':',array(aini:aend)
      !print*, name,'in final, j=',j,':',final(minix:mendx, my)
      !print*,rk,x,y,'->','(',miniy,':',mendy,',',mx,')'
      !print*,rk,x,y,'->','(',minix,':',mendx,',',my,')'

    end do
  end subroutine fill_matrix
        


  subroutine open_newfile(u,filename)
    integer, intent(in) :: u
    character(len=*), intent(in) :: filename
    integer :: stat
    open(unit=u, iostat=stat, file=trim(filename), status='old')
    if (stat == 0) close(u, status='delete')
    open(unit=u,file=trim(filename),status='new')
  end subroutine

  subroutine open_newfile_unformatted(u,filename)
    integer, intent(in) :: u
    character(len=*), intent(in) :: filename
    integer :: stat
    open(unit=u, iostat=stat, file=trim(filename), status='old')
    if (stat == 0) close(u, status='delete')
    open(unit=u,file=trim(filename),status='new',form='unformatted',access='stream',convert='big_endian')
  end subroutine

end module

program main
  use solve_diffusion 
  implicit none

  integer :: it,i,j
  integer :: N = 50
  character(len=80) :: namefile
  double precision, dimension(:,:), allocatable :: phi
  double precision, dimension(:), allocatable :: phi_t,phi_b,phi_l,phi_r

  allocate(phi(N,N))
  allocate(phi_t(N))
  allocate(phi_b(N))
  allocate(phi_r(N))
  allocate(phi_l(N))

  phi=0.0d0
  phi_t = 1.0d0
  phi_b = 1.0d0
  phi_l = 1.0d0
  phi_r = 1.0d0

  do it=0,100
    call solve_diff(phi,phi_t,phi_l,phi_b,phi_r)

    write(namefile,'(a,i8.8,a)') './output/OUTPAR_',it,'.dat'
    call open_newfile(76,trim(namefile))!,status='new',form='unformatted',access='stream',convert='big_endian')
    do j=1,N
      do i=1,N
        write(76,*) i,j,phi(i,j)
      end do
      write(76,*)
    end do
  end do

contains

  subroutine open_newfile(u,filename)
    integer, intent(in) :: u
    character(len=*), intent(in) :: filename
    integer :: stat
    open(unit=u, iostat=stat, file=trim(filename), status='old')
    if (stat == 0) close(u, status='delete')
    open(unit=u,file=trim(filename),status='new')
  end subroutine


end program main


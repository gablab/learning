!!! ACHTUNG !!!
! the following code has issues, it is didactical


program main

  use parallel_io
  use mpi_structures
  implicit none

  integer :: comm, info, fh, ierror, amode
  character(100) :: filename
  integer(kind=mpi_offset_kind) :: displacement
  integer :: stat(MPI_STATUS_SIZE)
  integer :: rank, nproc
  integer :: int_size, data_size
  integer :: i

  integer :: readsize 
  integer, dimension(:), allocatable :: readbuf

  double precision, dimension(5) :: tosubarr
  integer :: subarr

  double precision, dimension(20) :: myarr = (/ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 /)
  double precision, dimension(20) :: rdarr

  call mpi_init(ierror)
  call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)


  call mpi_type_create_subarray(1,(/20/),(/5/),&  
    (/rank*5/),&
    MPI_ORDER_FORTRAN,MPI_DOUBLE_PRECISION,subarr,ierror)
  call mpi_type_commit(subarr,ierror)

  do i=1,20
    tosubarr(i) = rank*10+dble(i)
  end do
  print*, rank, tosubarr

  filename = 'test.dat'
  
  amode = ior(MPI_MODE_RDWR,MPI_MODE_CREATE)
  info = MPI_INFO_NULL
  comm = MPI_COMM_WORLD
  call mpi_type_size(MPI_DOUBLE_PRECISION,data_size,ierror)
  displacement = rank * data_size * 5
  call mpi_file_open(comm,trim(filename),amode,info,fh,ierror)
  call mpi_file_write(fh,tosubarr,1,subarr,stat,ierror)
  !writes only the first 5 for one processor !call mpi_file_write(fh,tosubarr,5,MPI_DOUBLE_PRECISION,stat,ierror)
  !call mpi_file_write_at(fh,displacement,tosubarr,5,MPI_DOUBLE_PRECISION,stat,ierror)
  call mpi_file_close(fh,ierror)

  call parallel_read_array_double(rdarr,trim(filename))
  if(rank.eq.0) print*, rdarr



  call mpi_finalize(ierror)


contains


end program

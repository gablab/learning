program main
  use mpi
  implicit none
  integer :: comm, info, fh, ierror, amode
  integer :: rank, nproc, crank
  integer :: next,prev
  integer :: i
  double precision, dimension(5) :: vs, vr

  call mpi_init(ierror)
  call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
  call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)

  next = rank+1
  if(next.ge.nproc) next = 0
  prev = rank-1
  if(prev.lt.0) prev = nproc-1

  do i=1,size(vs)
  vs(i) = rank**i
  end do
  print*,rank,'sends',vs

  if(mod(rank,2).eq.0) then
    call mpi_sendrecv(vs,size(vs),MPI_DOUBLE_PRECISION,next,13, &
      vr,size(vr),MPI_DOUBLE_PRECISION,prev,13,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierror)
  else
    call mpi_recv(vr,size(vr),MPI_DOUBLE_PRECISION,prev,13,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierror)
    call mpi_send(vs,size(vs),MPI_DOUBLE_PRECISION,next,13,MPI_COMM_WORLD,ierror)
  end if
  
  print*,rank,'received',vr





  call MPI_FINALIZE(ierror)

end program main

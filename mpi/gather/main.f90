! Learn MPI_GATHER
! DOES NOT BEHAVE LIKE EXPECTED!

program main
  use mpi
  implicit none
  integer :: comm, info, fh, ierror, amode
  integer :: rank, nproc, crank
  integer :: next,prev
  integer :: i,j
  integer, parameter :: sz = 2
  integer, dimension(sz,sz) :: vs
  integer, dimension(:,:), allocatable :: vr


  call mpi_init(ierror)
  call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)
  call mpi_comm_size(MPI_COMM_WORLD, nproc, ierror)
  allocate(vr(sz*nproc,sz*nproc))
  vr = 999

  do i=1,size(vs,1)
    do j=1,size(vs,2)
      vs(i,j) = rank*100 + 10 * i + 1 * j
    end do
  end do
  print*,rank,'sends',vs

  call MPI_GATHER(vs,sz,MPI_INTEGER,&
    vr,sz,MPI_INTEGER,&
    0,MPI_COMM_WORLD,ierror)


  if(rank.eq.0) then
    do i=1,size(vr,1)
      print*,'received',vr(i,:)
    end do
  end if





  call MPI_FINALIZE(ierror)

end program main

module modMatrices
contains
  subroutine system_solve(A,B,x,error,max_error) ! Ax=B
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(:), intent(in) :: B
    double precision, dimension(:), allocatable, intent(inout) :: x
    double precision, dimension(:), allocatable :: x_err
    double precision, dimension(:,:), allocatable :: A_work, B_work
    integer, dimension(:), allocatable :: IPIV
    integer :: N, NRHS, LDA, LDB
    double precision :: rms
    double precision, optional :: error,max_error

    N = size(A(:,1))
    LDA = size(A(1,:))
    if(N.ne.LDA) then
      write(*,*) "size of A: ",N,LDA
      stop "system_solve : The matrix to be inverted should be square!"
    end if
    NRHS = 1
    LDB = size(B)
    if(LDA.ne.LDB) then
      write(*,*) "size of A: ",N,LDA,"; size of B: ",NRHS,LDB
      stop "system_solve : Inconsistency of matrix dimensions!"
    end if

    ! prepare matrices to inversion
    call matr_copy(A,A_work)
    allocate(B_work(LDB,1))
    B_work(:,1) = B
    allocate(IPIV(N))

    ! call Lapack
    call DGESV(N, NRHS, A_work, LDA, IPIV, B_work, LDB, info)

    ! copy result to x
    if(allocated(x)) deallocate(x)
    allocate(x(LDB))
    x = B_work(:,1)

    ! check result
    x_err = linapp(A,x)-B
    x_err = x_err**2
    rms = sqrt(sum(x_err))/LDB
    if(present(max_error)) then
      if(rms.gt.max_error) then
        write(*,*) "system_solve : global error = ",rms,", bigger than threshold"
      end if
    end if
    if(present(error)) error = rms
  end subroutine

  subroutine print_matrix(M,filename)
    double precision, dimension (:,:) :: M
    character (len=*), optional :: filename

    if(present(filename)) then
      open(66,file=trim(filename))
      do i=1,size(M(:,1))
        write(66,*) M(i,:)
      end do
    else
      do i=1,size(M(:,1))
        write(*,*) M(i,:)
      end do
    end if
  end subroutine

  subroutine matr_casual(M,n)
    double precision, dimension (:,:), allocatable, intent(inout) :: M
    integer, intent(in) :: n
    double precision, dimension (:), allocatable :: arr
    integer :: i

    if(allocated(M)) deallocate(M)
    allocate(M(n,n))
    allocate(arr(n**2))

    do i=1,n*n
      arr(i) = atanh(RAND())
    end do

    M = transpose(reshape(arr,[n,n]))
    deallocate(arr)
  end subroutine 

  subroutine matr_copy(A,B)
    double precision, dimension (:,:), intent(in) :: A
    double precision, dimension (:,:), allocatable :: B
    integer :: m,n,i,j

    m = size(A(:,1))
    n = size(A(1,:))

    if(allocated(B)) deallocate(B)
    allocate(B(m,n))

    do i=1,m
      do j=1,n
        B(i,j) = A(i,j)
      end do
    end do
  end subroutine 

  function linapp(A,x) result(y)
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), allocatable :: y
    integer :: m,n,i

    m = size(A(:,1))
    n = size(A(1,:))
    if(n.ne.size(x)) stop "linapp : Dimension of x not consistent!"
    allocate(y(n))

    do i=1,n
      y(i) = dot_product(A(i,:),x)
    end do

  end function

end module

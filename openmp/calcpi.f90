! COMPILING:
! >> gfortran -fopenmp -cpp -Dmode=... calcpi.f90
! values for mode:
! _ 1: reduce by hand at the end of the loop
! _ 2: use atomic
! _ 3: use native reduction
program main
  implicit none
  !$ include 'omp_lib.h'
  double precision :: pi_calc, dx, t1, t2, x_i
#if mode == 1 
  double precision :: pi_tmp
#endif
  double precision, dimension(:), allocatable :: pi_arr 
  double precision, parameter :: pi_math = 4.D0*DATAN(1.D0)
  integer :: n_intervals, exp_intervals, n_threads, exp_threads
  integer :: id_thread, intervals_per_thread
  integer :: i



  write(*,*)'threads intervals time error'

  do exp_intervals=4,24,4
    n_intervals = 2**exp_intervals
    do exp_threads=0,3
      n_threads = 2**exp_threads


      t1 = omp_get_wtime()
#if mode != 1
      pi_calc = 0.0d0
#endif
      dx = 1.0d0 / n_intervals

      call omp_set_num_threads(n_threads)
#if mode == 1
      allocate(pi_arr(n_threads))
#endif


#if mode == 1 
      !$omp parallel private(id_thread,x_i,pi_tmp)
      pi_tmp = 0.0d0
#elif mode == 2
      !$omp parallel private(id_thread,x_i)
#endif

#if mode != 3
      id_thread = omp_get_thread_num()
      intervals_per_thread = n_intervals / n_threads
      do i=id_thread*intervals_per_thread+1,(id_thread+1)*intervals_per_thread
#else
      !$omp parallel do reduction(+:pi_calc) private(x_i)
      do i=1,n_intervals
#endif
        x_i = (i-.5d0)*dx
#if mode == 2
        !$omp atomic
#endif
        pi_calc = pi_calc + 4.0d0*dx/(1+x_i**2)
#if mode == 2
        !$omp end atomic
#endif
#if mode == 1
        pi_tmp = pi_tmp + 4.0d0*dx/(1+x_i**2)
#endif
      end do
#if mode == 1
        pi_arr(id_thread+1) = pi_tmp
#endif

#if mode != 3
      !$omp end parallel
#endif

#if mode == 1
      pi_calc = sum(pi_arr)
      deallocate(pi_arr)
#endif

      t2 = omp_get_wtime()

      print*,n_threads,n_intervals,t2-t1,abs((pi_calc-pi_math)/pi_math),pi_calc

    end do 
  end do



end program main

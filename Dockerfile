FROM debian:latest
MAINTAINER Gabriele Labanca

WORKDIR /root

RUN apt-get update
RUN apt-get -y install apt-utils libapt-inst2.0


# ssh configuration
# https://techytok.ml/from-zero-to-julia/#conclusions : for the file
ARG SSH_PRIVATE_KEY
RUN mkdir .ssh/
RUN echo "${SSH_PRIVATE_KEY}" > .ssh/id_rsa
RUN echo "${SSH_PUBLIC_KEY}" > .ssh/id_rsa.pub
RUN echo "${SSH_CONFIG}" > .ssh/config
RUN chmod a=-rwx .ssh/id_rsa
RUN chmod u=r    .ssh/id_rsa
RUN apt-get install -y openssh-server

RUN apt-get install -y git

RUN git config --global user.email gabrielelabanca@gmail.com \
  && git config --global user.name GabrieleLabanca
#RUN GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone git@gitlab.com:gablab/learning.git
#RUN GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone git@gitlab.com:gablab/fortran_utils.git

RUN echo "git clone git@gitlab.com:gablab/learning.git" >> cloning.sh
RUN echo "git clone git@gitlab.com:gablab/fortran_utils.git" >> cloning.sh



RUN apt-get install -y make vim g++ wget \
  gfortran gnuplot xauth fftw3 libfftw3-dev libfftw3-doc \
  liblapack3 liblapack-dev \
  imagemagick \
  mpich libscalapack-mpi-dev
  #openmpi-bin libopenmpi-dev

RUN wget https://raw.githubusercontent.com/GabrieleLabanca/dotfiles/master/.vimrc

RUN apt-get install -y curl zsh
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"



RUN alias mympi='mpirun --allow-run-as-root'



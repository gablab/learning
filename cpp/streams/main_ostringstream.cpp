// reading a text file
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

void write_to_string(ostream & stream){
  stream << "Nel mezzo del cammin di nostra vita" << endl;
  stream << "mi ritrovai per una selva oscura" << endl;
  stream << "ché la diritta via era smarrita." << endl;
}

int main () {
  ostringstream ss; 
  ss << "COMMEDIA" << endl;
  write_to_string(ss);
  cout << ss.str() << endl;

  return 0;
}


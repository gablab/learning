# Cut Cat

Take the picture of a cat and cut some frequencies.

## Compilation
Execute the file 'run.sh', which contains the following:
```
rm -f *.out *.mod *.o

gfortran -c -I/usr/include FFTW3.f03

gfortran -lfftw3 main.f03 
```

## Execution 
To convert the image into a raw file
```
convert cat1.jpg -separate -swap 1,2 -combine out.txt
```

Then simply run the `a.out` executable, then do the following

```
cat out.txt | awk '{print $2}' | sed 's/(//g' | sed 's/,/ /g' | sed 's/)//g' | awk '{ print $1}' > raw.dat
```

## Visualize results
To visualize the results, for example in Gnuplot, use the following command:
```
plot "out_tf_tf.dat" matrix with image
```


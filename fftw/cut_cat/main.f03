program main

  use FFTW3
  implicit none


  integer :: i,j, tmp
  integer, parameter :: N = 150
  double precision, parameter :: pi = 4 * atan (1.0_16)
  complex(C_DOUBLE_COMPLEX), dimension(N,N) :: input, output
  type(C_PTR) :: plan

  open(66,file="raw.dat")
  do j=1,N
  do i=1,N
  read(66,*) tmp
  input(N+1-j,i) = dble(tmp)
    !input(i,:) = dsin(dble(i)*pi*2/N) + dsin(dble(i)*2*pi*3/N) + dsin(dble(i)*2*pi*10/N)
  end do
  end do
  close(66)

  open(66,file="out.dat")
  do i=1,N
    write(66,*) real(input(i,:))!, input(i)%im
  end do
  close(66)

  plan = fftw_plan_dft_2d(N,N, input,output, FFTW_FORWARD,FFTW_ESTIMATE)
  call fftw_execute_dft(plan, input, output)
  call fftw_destroy_plan(plan)
  !output(5:N-5,:) = 0.0 !low-pass
  output(1:5,:) = 0.0 !high-pass 1/2
  output(N-5:,:) = 0.0 !high-pass 2/2
  plan = fftw_plan_dft_2d(N,N, output,output, FFTW_BACKWARD,FFTW_ESTIMATE)
  call fftw_execute_dft(plan, output, input)
  input = input/dble(N)
  call fftw_destroy_plan(plan)


  open(66,file="out_tf.dat")
  do i=1,N
    write(66,*) real(output(i,:)) !, aimag(output(i))
  end do
  close(66)

  open(66,file="out_tf_tf.dat")
  do i=1,N
    write(66,*) real(input(i,:))!, aimag(input(i))
  end do
  close(66)


end program

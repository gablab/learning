<https://learnopengl.com>

On Mac, `brew install glfw`. Go to <https://glad.dav1d.de/> and generate files: the `gl` field should be set to 3.3 (it will be set like so also in `test_glad.cpp`.
Put the files in the compile directory, with the structure 
```
project
├── KHR
│   └── khrplatform.h
├── a.out
└── glad
    ├── glad.h
    └── src
        └── glad.c
```

```
g++ -I. -I/usr/local/include/GLFW/ -L/usr/local/lib/  -lglfw.3 glad/src/glad.c test_glad.cpp
```



#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

int main()
{
  glfwInit();
  // Set OpenGL version
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  // core-profile means access to a smaller subset of OpenGL features without backwards-compatible (and not-needed) features
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  // On Mac OS X the following line is needed
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

  GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
  if (window == NULL)
  {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "Failed to initialize GLAD" << std::endl;
    return -1;
  }  

  glViewport(0, 0, 800, 600);
  // set callback function for resizing
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);  

  // keep the window open
  while(!glfwWindowShouldClose(window))
  {    
    processInput(window);

    // rendering
    // set color for clear
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    // clear screen
    glClear(GL_COLOR_BUFFER_BIT);

    // a double buffer is used: when the back is updated, swap
    glfwSwapBuffers(window);
    // check if any event is triggered
    glfwPollEvents();    
  }

  glfwTerminate();

  return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
}

// keep all input code together
void processInput(GLFWwindow *window)
{
  // if ESC is pressed, close window
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
}

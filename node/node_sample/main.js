// https://www.w3schools.com/nodejs

var http = require('http');
var url = require('url');
var fs = require('fs');

var dt = require('./js/myfirstmodule');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write("The date and time are currently: " + dt.myDateTime() + '\n');
  res.write(req.url + '\n')
  var q = url.parse(req.url, true).query;
  res.write(q.variable + '\n')
  fs.readFile('index.html', function(err, data) {
    res.write(data);
    return res.end();
  });
}).listen(8080); 

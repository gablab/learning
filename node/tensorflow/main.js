const tf = require('@tensorflow/tfjs');
const fetch = require('node-fetch');

// Optional Load the binding:
// Use '@tensorflow/tfjs-node-gpu' if running with GPU.
require('@tensorflow/tfjs-node');


function createModel() {
  // Create a sequential model
  const model = tf.sequential();

  // Add a single input layer
  model.add(tf.layers.dense({inputShape: [1], units: 1, useBias: true}));

  model.add(tf.layers.dense({units: 5, activation: 'sigmoid'}));
  model.add(tf.layers.dense({units: 25, activation: 'sigmoid'}));
  model.add(tf.layers.dense({units: 50, activation: 'sigmoid'}));
  model.add(tf.layers.dense({units: 25, activation: 'sigmoid'}));
  model.add(tf.layers.dense({units: 5, activation: 'sigmoid'}));


  // Add an output layer
  model.add(tf.layers.dense({units: 1, useBias: true}));


  return model;
}


async function getData() {
  const carsDataResponse = await fetch('https://storage.googleapis.com/tfjs-tutorials/carsData.json');
  const carsData = await carsDataResponse.json();
  const cleaned = carsData.map(car => ({
    mpg: car.Miles_per_Gallon,
    horsepower: car.Horsepower,
  }))
    .filter(car => (car.mpg != null && car.horsepower != null));

  return cleaned;
}


function convertToTensor(data) {
  // Wrapping these calculations in a tidy will dispose any
  // intermediate tensors.
  

  return tf.tidy(() => {
    // Step 1. Shuffle the data
    tf.util.shuffle(data);

    // Step 2. Convert data to Tensor
    const inputs = data.map(d => d.horsepower)
    const labels = data.map(d => d.mpg);

    const inputTensor = tf.tensor2d(inputs, [inputs.length, 1]);
    const labelTensor = tf.tensor2d(labels, [labels.length, 1]);

    //Step 3. Normalize the data to the range 0 - 1 using min-max scaling
    const inputMax = inputTensor.max();
    const inputMin = inputTensor.min();
    const labelMax = labelTensor.max();
    const labelMin = labelTensor.min();

    const normalizedInputs = inputTensor.sub(inputMin).div(inputMax.sub(inputMin));
    const normalizedLabels = labelTensor.sub(labelMin).div(labelMax.sub(labelMin));

    return {
      inputs: normalizedInputs,
      labels: normalizedLabels,
      // Return the min/max bounds so we can use them later.
      inputMax,
      inputMin,
      labelMax,
      labelMin,
    }
  });
}


async function trainModel(model, inputs, labels) {
  // Prepare the model for training.
  model.compile({
    optimizer: tf.train.adam(),
    loss: tf.losses.meanSquaredError,
    metrics: ['mse'],
  });

  const batchSize = 32;
  const epochs = 200;

  return await model.fit(inputs, labels, {
    batchSize,
    epochs,
    shuffle: true
  });
}

function testModel(model, testData, normalizationData) {
  const {inputMax, inputMin, labelMin, labelMax} = normalizationData;

  // Generate predictions for a uniform range of numbers between 0 and 1;
  // We un-normalize the data by doing the inverse of the min-max scaling
  // that we did earlier.
  const [xs, ys, preds] = tf.tidy(() => {

    const xs = testData['inputsTest'];
    const ys = testData['labelsTest'];
    const preds = model.predict(xs);

    const unNormXs = xs
      .mul(inputMax.sub(inputMin))
      .add(inputMin);

    const unNormYs = ys
      .mul(labelMax.sub(labelMin))
      .add(labelMin);

    const unNormPreds = preds
      .mul(labelMax.sub(labelMin))
      .add(labelMin);

    // Un-normalize the data
    return [unNormXs.dataSync(), unNormYs.dataSync(), unNormPreds.dataSync()];
  });

  //var squared_diffs = tf.math.square(tf.(ys, preds);
  //var mean_squared_diff = tf.math.sum(squared_diffs)/squared_diffs.length;
  var mean_squared_diff = tf.losses.absoluteDifference (ys, preds);
  console.log("Error on test set = " + mean_squared_diff);


  /*const predictedPoints = Array.from(xs).map((val, i) => {
    return {x: val, y: preds[i]}
  });

  const originalPoints = inputData.map(d => ({
    x: d.horsepower, y: d.mpg,
  }));*/
}



(async function run() {
  const data = await getData();
  
  tf.util.shuffle(data);

  nTrain = Math.floor(0.85 * data.length)
  //nTest = data.length - nTrain

  // Convert the data to a form we can use for training.
  const tensorDataTrain = convertToTensor(data.slice(0,nTrain));
  const {inputs, labels} = tensorDataTrain;
  
  const tensorDataTest = convertToTensor(data.slice(nTrain));
  const inputsTest = tensorDataTest['inputs'];
  const labelsTest = tensorDataTest['labels'];


  const model = createModel();
  

  // Train the model
  await trainModel(model, inputs, labels);
  console.log('Done Training');

  // Make some predictions using the model and compare them to the
  // original data
  testModel(model, {inputsTest, labelsTest}, tensorDataTest);


})()



program main
  use modMatrices
  implicit none

  double precision, dimension(:,:), allocatable :: A,B,A_work,P
  double precision, dimension(:), allocatable :: Q,y
  integer :: info,i,j
  integer, dimension(:), allocatable :: IPIV
  integer :: N, NRHS, LDA, LDB


  ! https://doi.org/10.1137/1014099 example 1.4
  LDA = 2
  N = 2
  allocate(A(LDA,N))
  allocate(A_work(LDA,N))
  A = transpose(reshape([0.01,1.0,1.0,101.0],[N,LDA]))
  A_work = A
  LDB = 2
  NRHS = 1
  allocate(B(LDB,NRHS))
  B = transpose(reshape([0.1,11.0],[NRHS,LDB]))
  allocate(IPIV(3))


  write(*,*) "A"
  call print_matrix(A)
  write(*,*) "B"
  call print_matrix(B)

  call DGESV(N, NRHS, A_work, LDA, IPIV, B, LDB, info)


  IF (info .EQ. 0) THEN
    write(*,*) "RESULT (should be -90,1)"
    call print_matrix(B)
  else 
    write(*,*) "ERROR"
  END IF

  write(*,*) "check"
  call print_matrix(matmul(A,B))

  !!!!!!!!!!!!
  write(*,*) 

  deallocate(A)
  allocate(A(2,2))
  write(*,*) "check_A"
  A = transpose(reshape([1.0,2.0,3.0,4.0],[2,2]))
  call print_matrix(A)
  deallocate(B)
  allocate(B(2,2))
  write(*,*) "check_B"
  B = transpose(reshape([0.0,1.0,-1.0,0.0],[2,2]))
  call print_matrix(B)
  write(*,*) "check_A*B"
  call print_matrix(matmul(A,B))
  

  !!!!!!!!!!!!!!!
  write(*,*) 

  deallocate(A)
  allocate(A(2,2))
  write(*,*) "check_A"
  A = transpose(reshape([1.0,2.0,3.0,4.0],[2,2]))
  call print_matrix(A)
  deallocate(B)
  allocate(B(2,1))
  write(*,*) "check_B"
  B = transpose(reshape([2.0,1.0],[1,2]))
  call print_matrix(B)
  write(*,*) "check_A*B"
  call print_matrix(matmul(A,B))


  allocate(P(3,3))
  P = transpose(reshape([1,2,3,4,5,6,7,8,9],[3,3]))
  allocate(Q(3))
  Q = [1.1,1.2,1.3]



  call system_solve(P,Q,y,max_error=1.0d-5)
  write(*,*) "check subroutine"
  write(*,*) (linapp(P,y)-Q)!,"b_residuals_subroutine.dat")

contains


end program


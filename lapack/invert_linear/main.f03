program main
  implicit none

  double precision, dimension (:,:), allocatable :: A,A_work,B,x
  integer :: info,i,j
  integer, dimension(:), allocatable :: IPIV
  integer :: N, NRHS, LDA, LDB
  integer, parameter :: ngrid = 10

  call srand(233833)

  LDA = ngrid
  N = ngrid
  LDB = ngrid
  NRHS = ngrid

  call matr_casual(A,ngrid)
  call matr_copy(A,A_work)
  call matr_casual(B,ngrid)
  call matr_copy(B,x)

  allocate(IPIV(ngrid))

  write(*,*) "A"
  call print_matrix(A,"a.dat")
  write(*,*) "B"
  call print_matrix(B,"b.dat")

  call DGESV(N, NRHS, A_work, LDA, IPIV, x, LDB, info)

  IF (info .EQ. 0) THEN
    write(*,*) "RESULT"
    call print_matrix(x)
  else 
    write(*,*) "ERROR",info
  END IF

  write(*,*) "check"
  call print_matrix(matmul(A,x)-B,"b_residuals.dat")

contains

  subroutine system_solve(A,B,x) ! Ax=B
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(:), intent(in) :: B
    double precision, dimension(:), allocatable, intent(inout) :: x
    double precision, dimension(:,:), allocatable :: A_work, B_work
    integer, dimension(:), allocatable :: IPIV
    integer :: N, NRHS, LDA, LDB

    N = size(A(:,1))
    LDA = size(A(1,:))
    if(N.ne.LDA) then
      stop "system_solve : The matrix to be inverted should be square!"
    end if
    NRHS = 1
    LDB = size(B)
    if(LDA.ne.LDB) then
      stop "system_solve : Inconsistency of matrix dimensions!"
    end if

    ! prepare matrices to inversion
    call matr_copy(A,A_work)
    allocate(B_work(LDB,1))
    B_work(:,1) = B

    ! call Lapack
    call DGESV(N, NRHS, A_work, LDA, IPIV, B_work, LDB, info)

    ! copy result to x
    if(allocated(x)) deallocate(x)
    allocate(x(LDB))
    x = B_work(:,1)
  end subroutine





  subroutine print_matrix(M,filename)
    double precision, dimension (:,:) :: M
    character (len=*), optional :: filename

    if(present(filename)) then
      open(66,file=trim(filename))
      do i=1,size(M(:,1))
        write(66,*) M(i,:)
      end do
    else
      do i=1,size(M(:,1))
        write(*,*) M(i,:)
      end do
    end if
  end subroutine

  subroutine matr_casual(M,n)
    double precision, dimension (:,:), allocatable, intent(inout) :: M
    integer, intent(in) :: n
    double precision, dimension (:), allocatable :: arr
    integer :: i

    if(allocated(M)) deallocate(M)
    allocate(M(n,n))
    allocate(arr(n**2))

    do i=1,n*n
      arr(i) = atanh(RAND())
    end do

    M = transpose(reshape(arr,[n,n]))
    deallocate(arr)
  end subroutine 

  subroutine matr_copy(A,B)
    double precision, dimension (:,:), intent(in) :: A
    double precision, dimension (:,:), allocatable :: B
    integer :: m,n,i,j

    m = size(A(:,1))
    n = size(A(1,:))

    if(allocated(B)) deallocate(B)
    allocate(B(m,n))

    do i=1,m
      do j=1,n
        B(i,j) = A(i,j)
      end do
    end do
  end subroutine 


end program

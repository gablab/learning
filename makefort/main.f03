program main
  use module1
  use module2

  implicit none

  integer :: a, b

  a = 1
  b = 3 

  call func1(a)
  call func2(b)

#if flag_print_flagged == 1 
  write(*,*) "FLAGGED (should see it)"
#endif

#if flag_print_unflagged == 1
  write(*,*) "UNFLAGGED (should *NOT* see it!)"
#endif

end program

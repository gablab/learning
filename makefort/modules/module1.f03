module module1
        use constants      
        implicit none

contains
        subroutine func1(x)
                integer :: x
                write(*,*) x, "from func1"
        end subroutine
end module                
       

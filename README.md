```
sudo systemctl start docker.service
SSH_PRIVATE_KEY=`cat $HOME/.ssh/id_rsa`
SSH_PUBLIC_KEY=`cat $HOME/.ssh/id_rsa.pub`
SSH_CONFIG=`cat $HOME/.ssh/config`
docker image build --build-arg SSH_PRIVATE_KEY=${SSH_PRIVATE_KEY}/.ssh --build-arg SSH_PUBLIC_KEY=${SSH_PUBLIC_KEY}/.ssh --build-arg SSH_CONFIG=${SSH_CONFIG}/.ssh -t learning .
docker container run -it learning bash 
docker container ls --all
docker container rm --force learning # delete learning container
docker rm $(docker ps -a -q) # delete all containers

```
